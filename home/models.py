from django.db import models

# Create your models here.

"""
Пример простой модели, для создание модели можно создать класс и унаследовать Model
далее добавить атрибуты в класс как в примере, на основе этих атрибутов будут создаваться
столбцы в таблицы

После того как модель была прописана можно создать миграции, перед этим надо добавит приложение если не добавленно
в список INSTALLED_APPS в settings

>>> python3 manage.py makemigrations

Далее надо запустить миграции 

>>> python3 manage.py migrate

"""


class Student(models.Model):
    """
    Student data of all students in school
    """
    # id лучше указывать через AutoField это нужно для того чтобы при сохранении
    # инстанс который мы сохраняем подтянул сразу id
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)

    # связь один к одному указывается через OneToOneField, это поле принимает
    # два важных параметра то на какую модель делать связь (лучше указывать как строку)
    # и что делать при удалении
    # если глянуть под капот этого столбика можно увидеть что это ForeignKey
    book = models.OneToOneField('home.Book', on_delete=models.CASCADE, null=True)
    # связь один ко многим создается через ForeignKey
    subject = models.ForeignKey('home.Subject', on_delete=models.SET_NULL, null=True)


class Subject(models.Model):
    title = models.CharField(max_length=200)


class Teacher(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    # Ну и связь многое ко многим это ManyToManyField
    # в базе данных это выглядит как две один ко многим
    # привазаных к промежуточной табличке
    students = models.ManyToManyField('home.Student')


class Book(models.Model):

    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=200)
